# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:50+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Czech (Czech Republic) (https://www.transifex.com/fusiondirectory/teams/12202/cs_CZ/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs_CZ\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: admin/autofs/class_autofsManagement.inc:31
msgid "Auto fs"
msgstr "Auto fs"

#: admin/autofs/class_autofsManagement.inc:32
msgid "Auto fs management"
msgstr "Správa auto fs"

#: admin/autofs/class_nisObject.inc:26 admin/autofs/class_nisObject.inc:27
#: admin/autofs/class_nisObject.inc:30 admin/autofs/class_nisObject.inc:49
msgid "Directory"
msgstr "Adresář"

#: admin/autofs/class_nisObject.inc:53 admin/autofs/class_nisMap.inc:48
msgid "Name"
msgstr "název"

#: admin/autofs/class_nisObject.inc:53
msgid "Name of this directory"
msgstr "Název tohoto adresáře"

#: admin/autofs/class_nisObject.inc:59
msgid "Automount entry"
msgstr "Položka automatického připojení (automount)"

#: admin/autofs/class_nisObject.inc:59
msgid ""
"The entry of this directory for the automount daemon.\n"
" For instance 'auto.u' or '-fstype=nfs domaine.tld:/mount/directory'"
msgstr ""
"Položka této složky pro systémovou službu automount.\n"
"Například „auto.u“ nebo „-fstype=nfs nazevdomeny.tld:/pripojna/složka“"

#: admin/autofs/class_nisObject.inc:63 admin/autofs/class_nisMap.inc:26
#: admin/autofs/class_nisMap.inc:30 admin/autofs/class_nisMap.inc:44
msgid "Mount point"
msgstr "Přípojný bod"

#: admin/autofs/class_nisObject.inc:63
msgid "The mount point this directory will be placed in"
msgstr "Přípojný bod, do kterého bude tato složka umístěna"

#: admin/autofs/class_nisMap.inc:27
msgid "Autofs mount point"
msgstr "Přípojný bod pro autofs"

#: admin/autofs/class_nisMap.inc:48
msgid "Name of the mount point"
msgstr "Název přípojného bodu"

#: config/autofs/class_autofsConfig.inc:26
msgid "Autofs configuration"
msgstr "Nastavení Autofs"

#: config/autofs/class_autofsConfig.inc:27
msgid "FusionDirectory autofs plugin configuration"
msgstr "Nastavení zásuvného modulu autofs pro FusionDirectory"

#: config/autofs/class_autofsConfig.inc:40
msgid "AutoFS"
msgstr "AutoFS"

#: config/autofs/class_autofsConfig.inc:43
msgid "AutoFS RDN"
msgstr "Relativní rozlišený název AutoFS"

#: config/autofs/class_autofsConfig.inc:43
msgid "Branch in which automount info will be stored"
msgstr ""
"Větev, ve které budou ukládány údaje pro automatické připojování souborových"
" systémů"
