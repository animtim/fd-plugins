# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:05+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Swedish (https://www.transifex.com/fusiondirectory/teams/12202/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/user-reminder/class_userReminderConfig.inc:26
msgid "User reminder"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:27
msgid "Configuration for the reminder of accounts expiration"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:39
msgid "User reminder settings"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:42
msgid "Delay before expiration"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:42
msgid "Days before expiration to send the first mail"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:47
msgid "Delay before sending again"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:47
msgid "Days before sending a second mail (0 to disable)"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:52
msgid "Extension of the validity"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:52
msgid "Extension of the duration of the account in days"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:57
msgid "Sender email address"
msgstr "Avsändaradress för epost"

#: config/user-reminder/class_userReminderConfig.inc:57
msgid "Email address from which mails will be sent"
msgstr "Epostadress från vilken epostmeddelanden sänds"

#: config/user-reminder/class_userReminderConfig.inc:62
msgid "Allow use of alternate addresses"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:62
msgid ""
"Whether the field gosaMailAlternateAddress should be used as well to send "
"reminders"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:69
msgid "Ppolicy email settings"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:72
#: config/user-reminder/class_userReminderConfig.inc:98
msgid "Forward alerts to the manager"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:72
msgid "Forward ppolicy alerts to the manager"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:77
#: config/user-reminder/class_userReminderConfig.inc:103
#: config/user-reminder/class_userReminderConfig.inc:129
msgid "Subject"
msgstr "Ämne"

#: config/user-reminder/class_userReminderConfig.inc:77
msgid "Subject of the ppolicy alert email"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:79
msgid "[FusionDirectory] Your password is about to expire"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:82
#: config/user-reminder/class_userReminderConfig.inc:134
#, php-format
msgid "Body (%s are cn and login)"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:83
#, php-format
msgid ""
"Body of the ppolicy alert email, sent when the user password is about to "
"expire. Use %s for the cn and uid."
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:85
#, php-format
msgid ""
"Dear %1$s,\n"
"your password for account %2$s is about to expire, please change your password: \n"
"https://"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:95
msgid "Alert email settings"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:103
msgid "Subject of the alert email"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:105
msgid "[FusionDirectory] Your account is about to expire"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:108
#, php-format
msgid "Body (%s are cn, login, and link token)"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:109
#, php-format
msgid ""
"Body of the alert email, sent when the user is about to expire. Use %s for "
"the cn, uid and token."
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:111
#, php-format
msgid ""
"Dear %1$s,\n"
"your account %2$s is about to expire, please use this link to avoid this: \n"
"https://"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:121
msgid "Confirmation email settings"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:124
msgid "Forward confirmation to the manager"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:124
msgid "Forward account extension confirmation to the manager"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:129
msgid "Subject of the confirmation email"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:131
msgid "[FusionDirectory] Your account expiration has been postponed"
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:135
#, php-format
msgid ""
"Body of the confirmation email, sent when the user has postponed expiration."
" Use %s for the cn and uid."
msgstr ""

#: config/user-reminder/class_userReminderConfig.inc:137
#, php-format
msgid ""
"Dear %1$s,\n"
" your account %2$s expiration has been successfully postponed.\n"
msgstr ""

#: html/class_expiredUserPostpone.inc:59
msgid "This token is invalid"
msgstr ""

#: html/class_expiredUserPostpone.inc:202
msgid "Contact your administrator, there was a problem with mail server"
msgstr "Kontakta din administratör, det uppstod ett problem med epostservern"

#: html/class_expiredUserPostpone.inc:211
#, php-format
msgid "Did not find an account with login \"%s\""
msgstr ""

#: html/class_expiredUserPostpone.inc:214
#, php-format
msgid "Found multiple accounts with login \"%s\""
msgstr ""

#: ihtml/themes/breezy/user-reminder.tpl.c:2
#: ihtml/themes/breezy/user-reminder.tpl.c:5
msgid "User"
msgstr "Användare"

#: ihtml/themes/breezy/user-reminder.tpl.c:8
msgid "Expiration postpone"
msgstr ""

#: ihtml/themes/breezy/user-reminder.tpl.c:11
#: ihtml/themes/breezy/user-reminder.tpl.c:14
msgid "Success"
msgstr ""

#: ihtml/themes/breezy/user-reminder.tpl.c:17
msgid "Your expiration has been postponed successfully."
msgstr ""

#: ihtml/themes/breezy/user-reminder.tpl.c:20
#: ihtml/themes/breezy/user-reminder.tpl.c:23
msgid "Error"
msgstr "Fel"

#: ihtml/themes/breezy/user-reminder.tpl.c:26
msgid "There was a problem"
msgstr ""
