# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:57+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: German (https://www.transifex.com/fusiondirectory/teams/12202/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: addons/ldapmanager/tabs_ldif.inc:31
msgid "LDAP import/export"
msgstr "LDAP Import/Export"

#: addons/ldapmanager/tabs_ldif.inc:32
msgid "Allows the import or export of the ldap tree"
msgstr "Erlaubt das Importieren und Exportieren des LDAP-Baums"

#: addons/ldapmanager/tabs_ldif.inc:36
msgid "Ldap manager"
msgstr "LDAP-Manager"

#: addons/ldapmanager/class_ldapmanager.inc:45
msgid "LDIF"
msgstr "LDIF"

#: addons/ldapmanager/class_ldapmanager.inc:46
msgid "Export/Import the ldap tree to/from LDIF format"
msgstr "LDAP-Baum ins/vom LDIF-Format exportieren/importieren"

#: addons/ldapmanager/class_ldapmanager.inc:48
msgid "LDAP Manager"
msgstr "LDAP Manager"

#: addons/ldapmanager/class_ldapmanager.inc:61
#: addons/ldapmanager/class_ldapmanager.inc:74
#: addons/ldapmanager/class_ldapmanager.inc:94
msgid "Export"
msgstr "Export"

#: addons/ldapmanager/class_ldapmanager.inc:64
msgid "DN of a single entry to export as ldif"
msgstr ""

#: addons/ldapmanager/class_ldapmanager.inc:78
msgid "Export single entry"
msgstr "Exportiere einzelnen Eintrag"

#: addons/ldapmanager/class_ldapmanager.inc:81
msgid "Filter"
msgstr "Filter"

#: addons/ldapmanager/class_ldapmanager.inc:81
msgid "Filter to use for selecting objects to export"
msgstr ""

#: addons/ldapmanager/class_ldapmanager.inc:87
msgid ""
"Download a complete snapshot of the running LDAP directory for this base as "
"ldif"
msgstr ""

#: addons/ldapmanager/class_ldapmanager.inc:98
msgid "Export complete LDIF for"
msgstr "Exportiere vollständige LDIF-Datei für"

#: addons/ldapmanager/class_ldapmanager.inc:103
msgid "Import LDIF"
msgstr "LDIF importieren"

#: addons/ldapmanager/class_ldapmanager.inc:106
msgid "Overwrite existing entries"
msgstr "Vorhandene Einträge überschreiben"

#: addons/ldapmanager/class_ldapmanager.inc:106
msgid ""
"Remove fields that are not in the LDIF from the LDAP entries if they were "
"existing."
msgstr ""

#: addons/ldapmanager/class_ldapmanager.inc:111
msgid ""
"Import an LDIF file into your LDAP. Remember that FusionDirectory will not "
"check your LDIFs for FusionDirectory conformance."
msgstr ""

#: addons/ldapmanager/class_ldapmanager.inc:121
#: addons/ldapmanager/class_csvimport.inc:80
#: addons/ldapmanager/class_csvimport.inc:100
msgid "Import"
msgstr "Importieren"

#: addons/ldapmanager/class_ldapmanager.inc:125
msgid "Import LDIF file"
msgstr "LDIF-Datei importieren"

#: addons/ldapmanager/class_ldapmanager.inc:153
#: addons/ldapmanager/class_ldapmanager.inc:203
msgid "LDAP error"
msgstr "LDAP-Fehler"

#: addons/ldapmanager/class_ldapmanager.inc:154
#, php-format
msgid "No such object %s!"
msgstr "Objekt %s nicht gefunden!"

#: addons/ldapmanager/class_ldapmanager.inc:174
msgid "Permission error"
msgstr "Berechtigungsfehler"

#: addons/ldapmanager/class_ldapmanager.inc:175
#, php-format
msgid "You have no permission to export %s!"
msgstr "Sie haben keine Berechtigung, um %s zu exportieren!"

#: addons/ldapmanager/class_ldapmanager.inc:181
msgid "Error"
msgstr "Fehler"

#: addons/ldapmanager/class_ldapmanager.inc:182
#, php-format
msgid "Failed to generate ldap export, error was \"%s\"!"
msgstr ""

#: addons/ldapmanager/class_ldapmanager.inc:195
msgid "Warning"
msgstr "Warnung"

#: addons/ldapmanager/class_ldapmanager.inc:195
msgid ""
"Nothing to import, please upload a non-empty file or fill the textarea."
msgstr ""

#: addons/ldapmanager/class_ldapmanager.inc:201
#: addons/ldapmanager/class_csvimport.inc:247
msgid "Success"
msgstr "Erfolg"

#: addons/ldapmanager/class_ldapmanager.inc:201
#, php-format
msgid "%d entries successfully imported"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:29
msgid "CSV import"
msgstr "CSV-Import"

#: addons/ldapmanager/class_csvimport.inc:30
msgid "Import of csv data into the ldap tree"
msgstr "Import von CSV-Daten in den LDAP-Baum"

#: addons/ldapmanager/class_csvimport.inc:42
msgid "Import CSV"
msgstr "CSV importieren"

#: addons/ldapmanager/class_csvimport.inc:45
msgid "Object type"
msgstr "Objecttyp"

#: addons/ldapmanager/class_csvimport.inc:45
msgid "Type of objects you wish to import"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:51
msgid "Template"
msgstr "Vorlage"

#: addons/ldapmanager/class_csvimport.inc:51
msgid "Select a template to apply to imported entries"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:57
msgid "CSV file"
msgstr "CSV-Datei"

#: addons/ldapmanager/class_csvimport.inc:57
msgid "Import a CSV file into your LDAP"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:61
msgid "Separator"
msgstr "Feldtrenner"

#: addons/ldapmanager/class_csvimport.inc:61
msgid "Character used as separator in the CSV file"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:71
msgid "Fixed values"
msgstr "Feste Werte"

#: addons/ldapmanager/class_csvimport.inc:71
msgid ""
"Some fixed values that you might wanna use in the filling of the template."
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:87
msgid "Template filling"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:90
msgid "Select fields associations"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:240
#, php-format
msgid "Import failed for line %d"
msgstr ""

#: addons/ldapmanager/class_csvimport.inc:247
#, php-format
msgid "Successfully imported %d entries"
msgstr "%d Einträge erfolgreich importiert"
