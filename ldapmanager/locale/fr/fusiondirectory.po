# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# Benoit Mortier <benoit.mortier@opensides.be>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:57+0000\n"
"Last-Translator: Benoit Mortier <benoit.mortier@opensides.be>, 2018\n"
"Language-Team: French (https://www.transifex.com/fusiondirectory/teams/12202/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: addons/ldapmanager/tabs_ldif.inc:31
msgid "LDAP import/export"
msgstr "Import / Export LDAP"

#: addons/ldapmanager/tabs_ldif.inc:32
msgid "Allows the import or export of the ldap tree"
msgstr "Permet d'importer ou exporter des parties de votre arbre LDAP"

#: addons/ldapmanager/tabs_ldif.inc:36
msgid "Ldap manager"
msgstr "Gestion LDAP"

#: addons/ldapmanager/class_ldapmanager.inc:45
msgid "LDIF"
msgstr "LDIF"

#: addons/ldapmanager/class_ldapmanager.inc:46
msgid "Export/Import the ldap tree to/from LDIF format"
msgstr "Exporter/Importer l'arbre LDAP au format LDIF"

#: addons/ldapmanager/class_ldapmanager.inc:48
msgid "LDAP Manager"
msgstr "Gestion LDAP"

#: addons/ldapmanager/class_ldapmanager.inc:61
#: addons/ldapmanager/class_ldapmanager.inc:74
#: addons/ldapmanager/class_ldapmanager.inc:94
msgid "Export"
msgstr "Export"

#: addons/ldapmanager/class_ldapmanager.inc:64
msgid "DN of a single entry to export as ldif"
msgstr "DN d'une entrée à exporter comme LDIF"

#: addons/ldapmanager/class_ldapmanager.inc:78
msgid "Export single entry"
msgstr "Exporter une seule entrée"

#: addons/ldapmanager/class_ldapmanager.inc:81
msgid "Filter"
msgstr "Filtre"

#: addons/ldapmanager/class_ldapmanager.inc:81
msgid "Filter to use for selecting objects to export"
msgstr "Filtre à utiliser pour sélectionner les objets à exporter"

#: addons/ldapmanager/class_ldapmanager.inc:87
msgid ""
"Download a complete snapshot of the running LDAP directory for this base as "
"ldif"
msgstr ""
"Télécharger un instantané complet de l'annuaire LDAP pour cette base au "
"format LDIF"

#: addons/ldapmanager/class_ldapmanager.inc:98
msgid "Export complete LDIF for"
msgstr "Exporter un LDIF complet pour "

#: addons/ldapmanager/class_ldapmanager.inc:103
msgid "Import LDIF"
msgstr "Import LDIF"

#: addons/ldapmanager/class_ldapmanager.inc:106
msgid "Overwrite existing entries"
msgstr "Écraser les entrées existantes"

#: addons/ldapmanager/class_ldapmanager.inc:106
msgid ""
"Remove fields that are not in the LDIF from the LDAP entries if they were "
"existing."
msgstr ""
"Enlever les champs qui ne sont pas dans le LDIF de l'annuaire LDAP si ils "
"existaient."

#: addons/ldapmanager/class_ldapmanager.inc:111
msgid ""
"Import an LDIF file into your LDAP. Remember that FusionDirectory will not "
"check your LDIFs for FusionDirectory conformance."
msgstr ""
"Importer un fichier LDIF dans votre annuaire. Souvenez-vous que "
"FusionDirectory ne vérifiera pas si votre fichier est conforme à "
"l’organisation utilisée par FusionDirectory "

#: addons/ldapmanager/class_ldapmanager.inc:121
#: addons/ldapmanager/class_csvimport.inc:80
#: addons/ldapmanager/class_csvimport.inc:100
msgid "Import"
msgstr "Importer"

#: addons/ldapmanager/class_ldapmanager.inc:125
msgid "Import LDIF file"
msgstr "Import d'un fichier LDIF"

#: addons/ldapmanager/class_ldapmanager.inc:153
#: addons/ldapmanager/class_ldapmanager.inc:203
msgid "LDAP error"
msgstr "Erreur LDAP"

#: addons/ldapmanager/class_ldapmanager.inc:154
#, php-format
msgid "No such object %s!"
msgstr "Pas d'objet %s !"

#: addons/ldapmanager/class_ldapmanager.inc:174
msgid "Permission error"
msgstr "Erreur de permissions"

#: addons/ldapmanager/class_ldapmanager.inc:175
#, php-format
msgid "You have no permission to export %s!"
msgstr "Vous n'avez pas l'autorisation d'exporter %s !"

#: addons/ldapmanager/class_ldapmanager.inc:181
msgid "Error"
msgstr "Erreur"

#: addons/ldapmanager/class_ldapmanager.inc:182
#, php-format
msgid "Failed to generate ldap export, error was \"%s\"!"
msgstr ""
"Échec de la création d'une exportation de votre annuaire ldap, l'erreur est "
"«%s» !"

#: addons/ldapmanager/class_ldapmanager.inc:195
msgid "Warning"
msgstr "Avertissement"

#: addons/ldapmanager/class_ldapmanager.inc:195
msgid ""
"Nothing to import, please upload a non-empty file or fill the textarea."
msgstr ""
"Rien à importer, veuillez télécharger un fichier non vide ou remplir la zone"
" de texte."

#: addons/ldapmanager/class_ldapmanager.inc:201
#: addons/ldapmanager/class_csvimport.inc:247
msgid "Success"
msgstr "Réussi"

#: addons/ldapmanager/class_ldapmanager.inc:201
#, php-format
msgid "%d entries successfully imported"
msgstr "%d entrées importées avec succès"

#: addons/ldapmanager/class_csvimport.inc:29
msgid "CSV import"
msgstr "Import CSV"

#: addons/ldapmanager/class_csvimport.inc:30
msgid "Import of csv data into the ldap tree"
msgstr "Importer des données CSV dans votre annuaire LDAP"

#: addons/ldapmanager/class_csvimport.inc:42
msgid "Import CSV"
msgstr "Import CSV"

#: addons/ldapmanager/class_csvimport.inc:45
msgid "Object type"
msgstr "Type d'objet"

#: addons/ldapmanager/class_csvimport.inc:45
msgid "Type of objects you wish to import"
msgstr "Le type d'objet que vous voulez importer"

#: addons/ldapmanager/class_csvimport.inc:51
msgid "Template"
msgstr "Modèle"

#: addons/ldapmanager/class_csvimport.inc:51
msgid "Select a template to apply to imported entries"
msgstr "Sélectionnez un modèle à appliquer aux entrées importées"

#: addons/ldapmanager/class_csvimport.inc:57
msgid "CSV file"
msgstr "Fichier CSV"

#: addons/ldapmanager/class_csvimport.inc:57
msgid "Import a CSV file into your LDAP"
msgstr "Importer un fichier CSV dans votre LDAP"

#: addons/ldapmanager/class_csvimport.inc:61
msgid "Separator"
msgstr "Séparateur"

#: addons/ldapmanager/class_csvimport.inc:61
msgid "Character used as separator in the CSV file"
msgstr "Caractère utilisé comme séparateur dans votre fichier CSV"

#: addons/ldapmanager/class_csvimport.inc:71
msgid "Fixed values"
msgstr "Valeurs fixes"

#: addons/ldapmanager/class_csvimport.inc:71
msgid ""
"Some fixed values that you might wanna use in the filling of the template."
msgstr ""
"Certaines valeurs fixes que vous voulez utiliser dans le remplissage du "
"modèle."

#: addons/ldapmanager/class_csvimport.inc:87
msgid "Template filling"
msgstr "Remplissage du modèle"

#: addons/ldapmanager/class_csvimport.inc:90
msgid "Select fields associations"
msgstr "Sélectionnez l'association des champs"

#: addons/ldapmanager/class_csvimport.inc:240
#, php-format
msgid "Import failed for line %d"
msgstr "L'importation a échoué à la ligne %d"

#: addons/ldapmanager/class_csvimport.inc:247
#, php-format
msgid "Successfully imported %d entries"
msgstr "%d entrées importées avec succès"
