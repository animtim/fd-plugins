# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:00+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Russian (https://www.transifex.com/fusiondirectory/teams/12202/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: admin/ppolicy/class_ppolicyManagement.inc:29
msgid "Password policies"
msgstr ""

#: admin/ppolicy/class_ppolicyManagement.inc:30
msgid "Password policies management"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:27 admin/ppolicy/class_ppolicy.inc:31
#: personal/ppolicy/class_ppolicyAccount.inc:27
#: personal/ppolicy/class_ppolicyAccount.inc:42
msgid "Password policy"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:28
msgid "Password policy for ppolicy overlay"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:47
#: personal/ppolicy/class_ppolicyAccount.inc:45
msgid "Policy"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:52
msgid "Policy name"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:56
msgid "Description"
msgstr "Описание"

#: admin/ppolicy/class_ppolicy.inc:56
msgid "A short description of this policy"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:60
msgid "Minimum length"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:60
msgid ""
"Minimum length of the user supplied password - passwords shorter than this "
"value will be rejected"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:65
msgid "Passwords in history"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:65
msgid ""
"Number of passwords that are maintained in a list of previously used "
"passwords"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:70
msgid "Minimum password age"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:70
msgid "Minimum time between password changes"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:75
msgid "Maximum password age"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:75
msgid ""
"Maximum time a password is valid, after which it is deemed to be no longer "
"usable and any bind operations attempted with the expired password will be "
"treated as invalid"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:80
msgid "Expiry warning delay"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:80
msgid ""
"Defines the start time - in seconds - prior to the password expiry that "
"password expiry warning messages are returned in bind responses. 0 to "
"disable"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:85
msgid "Grace period"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:85
msgid ""
"Number of times a user is allowed to successfully bind using an expired "
"password"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:90
msgid "Allow user change"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:90
msgid "Whether users are allowed to change their own passwords"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:95
msgid "Safe modify"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:95
msgid ""
"Whether a user must send the current password during a password modification"
" operation"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:100
msgid "Check quality"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:100
msgid "Decides what to do if the function in \"Check module\" is not available"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:103
msgid "Disabled"
msgstr "Отключен"

#: admin/ppolicy/class_ppolicy.inc:103
msgid "Ignore errors"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:103
msgid "Reject on errors"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:106
msgid "Check module"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:106
msgid ""
"Name of a user supplied password quality check module that will be called to"
" perform password quality checks and is only relevant if pwdCheckQuality is "
"either 1 or 2"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:112
msgid "Lock out"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:116
msgid "Activate lock out"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:116
msgid ""
"Whether to lock an account that had more consecutive failed bind attempts "
"with invalid passwords than is defined by \"Maximum failures\""
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:121
msgid "Lock out duration"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:121
msgid ""
"Time the account remains locked after an automatic lock out (0 means for "
"ever)"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:126
msgid "Maximum failures"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:126
msgid ""
"Number of consecutive password failures allowed before automatic lock out"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:131
msgid "Failure count interval"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:131
msgid ""
"Time after which the count of consecutive password failures is reset even if"
" no successful authentication has occurred"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:136
msgid "Must change"
msgstr ""

#: admin/ppolicy/class_ppolicy.inc:136
msgid ""
"Whether the user must change their password after an account is reset by an "
"administrator following an automatic lockout"
msgstr ""

#: config/ppolicy/class_ppolicyConfig.inc:27
msgid "Ppolicy plugin configuration"
msgstr ""

#: config/ppolicy/class_ppolicyConfig.inc:28
msgid "FusionDirectory ppolicy plugin configuration"
msgstr ""

#: config/ppolicy/class_ppolicyConfig.inc:41
msgid "Ppolicy plugin"
msgstr ""

#: config/ppolicy/class_ppolicyConfig.inc:44
msgid "Ppolicy RDN"
msgstr ""

#: config/ppolicy/class_ppolicyConfig.inc:44
msgid "Branch in which ppolicies will be stored"
msgstr ""

#: config/ppolicy/class_ppolicyConfig.inc:49
msgid "Default ppolicy cn"
msgstr ""

#: config/ppolicy/class_ppolicyConfig.inc:49
msgid "What you put as default ppolicy in the overlay config"
msgstr ""

#: addons/dashboard/class_dashBoardPPolicy.inc:27
msgid "Ppolicy"
msgstr ""

#: addons/dashboard/class_dashBoardPPolicy.inc:28
msgid "Statistics about ppolicy expired users"
msgstr ""

#: addons/dashboard/class_dashBoardPPolicy.inc:40
msgid "Expired accounts"
msgstr "Просроченные учетные записи"

#: addons/dashboard/class_dashBoardPPolicy.inc:45
#: addons/dashboard/ppolicy_locked_accounts.tpl.c:8
msgid "Locked accounts"
msgstr ""

#: addons/dashboard/class_dashBoardPPolicy.inc:58
msgid "Login"
msgstr "Имя пользователя"

#: addons/dashboard/class_dashBoardPPolicy.inc:59
#: addons/dashboard/class_dashBoardPPolicy.inc:64
msgid "Name"
msgstr "Название"

#: addons/dashboard/class_dashBoardPPolicy.inc:60
#: addons/dashboard/class_dashBoardPPolicy.inc:66
msgid "Phone number"
msgstr "Телефон"

#: addons/dashboard/class_dashBoardPPolicy.inc:61
msgid "Expiration date"
msgstr "Дата окончания действия"

#: addons/dashboard/class_dashBoardPPolicy.inc:65
msgid "Email"
msgstr "Email"

#: addons/dashboard/class_dashBoardPPolicy.inc:86
#: addons/dashboard/class_dashBoardPPolicy.inc:139
msgid "Configuration error"
msgstr "Ошибка конфигурации"

#: addons/dashboard/class_dashBoardPPolicy.inc:87
#, php-format
msgid "Default ppolicy \"%s\" could not be found in the LDAP!"
msgstr ""

#: addons/dashboard/class_dashBoardPPolicy.inc:140
#, php-format
msgid "Ppolicy \"%s\" set for user \"%s\" could not be found in the LDAP!"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:28
msgid "Edit user's password policy"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:45
msgid "Use a specific policy for this user"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:49
msgid "Last password change"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:49
msgid "Last time the password for the entry was changed"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:53
msgid "Account locked time"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:53
msgid "Time the account was locked"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:57
msgid "Reset locking / force change"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:57
msgid "Resets the lock status of this account and/or force a password change"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:60
msgid "Force password change (resets locking)"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:60
msgid "Reset locking (same password)"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:75
msgid "Use the default"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:81
msgid "Never"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:88
msgid "Unlocked"
msgstr ""

#: personal/ppolicy/class_ppolicyAccount.inc:90
msgid "Locked permanently"
msgstr ""

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:2
msgid "There is one locked account"
msgid_plural "There are %1 locked accounts"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:5
msgid "There is no locked account"
msgstr ""

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:11
msgid "Manager concerned"
msgstr ""

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:14
msgid "uid"
msgstr "uid"

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:17
msgid "cn"
msgstr "cn"

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:20
#: addons/dashboard/ppolicy_locked_accounts.tpl.c:32
msgid "telephoneNumber"
msgstr ""

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:23
msgid "pwdAccountLockedTime"
msgstr ""

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:26
msgid "manager"
msgstr ""

#: addons/dashboard/ppolicy_locked_accounts.tpl.c:29
msgid "mail"
msgstr ""
