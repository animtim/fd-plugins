# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:59+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Arabic (https://www.transifex.com/fusiondirectory/teams/12202/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: admin/systems/services/postfix/class_servicePostfix.inc:54
#: admin/systems/services/postfix/class_servicePostfix.inc:55
msgid "Postfix (SMTP)"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:55
msgid "Services"
msgstr "خدمات"

#: admin/systems/services/postfix/class_servicePostfix.inc:70
msgid "Information"
msgstr "معلومات"

#: admin/systems/services/postfix/class_servicePostfix.inc:73
msgid "Hostname"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:73
msgid "The host name."
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:78
#: admin/systems/services/postfix/class_servicePostfix.inc:130
msgid "Domain"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:78
msgid "The domain."
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:83
msgid "Max mail header size (KB)"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:83
msgid "This value specifies the maximal header size."
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:88
msgid "Max mailbox size (KB)"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:88
msgid "Defines the maximal size of mail box."
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:93
msgid "Max message size (KB)"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:93
msgid "Specify the maximal size of a message."
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:100
msgid "Domains and routing"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:104
msgid "Domains to accept mail for"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:104
msgid "Postfix networks"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:109
msgid "Transport table"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:114
msgid "Transport rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:126
msgid "Catchall table"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:130
msgid "Domain concerned by this catchall rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:134
msgid "Recipient"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:134
msgid "Recipient mail address for this catchall rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:140
msgid "Domain alias table"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:144
msgid "From"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:144
msgid "Domain concerned by this alias rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:148
msgid "To"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:148
msgid "Recipient domain for this alias rule"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:156
msgid "Restrictions"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:160
msgid "Restrictions for sender"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:169
msgid "For sender"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:174
msgid "Restrictions for recipient"
msgstr ""

#: admin/systems/services/postfix/class_servicePostfix.inc:183
msgid "For recipient"
msgstr ""
