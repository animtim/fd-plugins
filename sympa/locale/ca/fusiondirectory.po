# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:05+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Catalan (https://www.transifex.com/fusiondirectory/teams/12202/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/services/sympa/class_serviceSympa.inc:27
#: admin/systems/services/sympa/class_serviceSympa.inc:28
msgid "Sympa Server"
msgstr ""

#: admin/systems/services/sympa/class_serviceSympa.inc:28
msgid "Services"
msgstr ""

#: admin/systems/services/sympa/class_serviceSympa.inc:43
#: admin/sympa/class_sympaAlias.inc:60
msgid "Sympa server"
msgstr ""

#: admin/systems/services/sympa/class_serviceSympa.inc:46
msgid "URL"
msgstr ""

#: admin/systems/services/sympa/class_serviceSympa.inc:46
msgid "URL to access the sympa server"
msgstr ""

#: admin/systems/services/sympa/class_serviceSympa.inc:50
msgid "User"
msgstr ""

#: admin/systems/services/sympa/class_serviceSympa.inc:50
msgid "User to access sympa server SOAP API."
msgstr ""

#: admin/systems/services/sympa/class_serviceSympa.inc:54
msgid "Password"
msgstr "Contrasenya"

#: admin/systems/services/sympa/class_serviceSympa.inc:54
msgid "Password to access sympa server SOAP API."
msgstr ""

#: admin/sympa/class_sympaManagement.inc:33
#: config/sympa/class_sympaConfig.inc:40
msgid "Sympa"
msgstr ""

#: admin/sympa/class_sympaManagement.inc:34
msgid "Sympa management"
msgstr ""

#: admin/sympa/class_sympaAlias.inc:26 admin/sympa/class_sympaAlias.inc:27
#: admin/sympa/class_sympaAlias.inc:30 admin/sympa/class_sympaAlias.inc:45
msgid "Sympa list alias"
msgstr ""

#: admin/sympa/class_sympaAlias.inc:48
msgid "Name"
msgstr "Nom"

#: admin/sympa/class_sympaAlias.inc:48
msgid "Name to identify this alias"
msgstr ""

#: admin/sympa/class_sympaAlias.inc:50
msgid "Description"
msgstr ""

#: admin/sympa/class_sympaAlias.inc:50
msgid "Description of this alias"
msgstr ""

#: admin/sympa/class_sympaAlias.inc:55
msgid "Email address"
msgstr ""

#: admin/sympa/class_sympaAlias.inc:60
msgid "Sympa server for this alias"
msgstr ""

#: config/sympa/class_sympaConfig.inc:26
msgid "Sympa configuration"
msgstr ""

#: config/sympa/class_sympaConfig.inc:27
msgid "FusionDirectory sympa plugin configuration"
msgstr ""

#: config/sympa/class_sympaConfig.inc:43
msgid "Sympa RDN"
msgstr ""

#: config/sympa/class_sympaConfig.inc:43
msgid "Branch in which Sympa objects will be stored"
msgstr ""
