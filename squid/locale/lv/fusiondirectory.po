# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:03+0000\n"
"Language-Team: Latvian (https://www.transifex.com/fusiondirectory/teams/12202/lv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: lv\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);\n"

#: personal/squid/class_proxyAccount.inc:58
msgid "Proxy"
msgstr ""

#: personal/squid/class_proxyAccount.inc:59
msgid "This allow to store basic squid settings in ldap for each user"
msgstr ""

#: personal/squid/class_proxyAccount.inc:78
msgid "Proxy account"
msgstr ""

#: personal/squid/class_proxyAccount.inc:84
msgid "Filter unwanted content (i.e. pornographic or violence related)"
msgstr ""

#: personal/squid/class_proxyAccount.inc:85
msgid "filterF"
msgstr ""

#: personal/squid/class_proxyAccount.inc:91
msgid "Working Start"
msgstr ""

#: personal/squid/class_proxyAccount.inc:94
#: personal/squid/class_proxyAccount.inc:109
msgid "HourStart"
msgstr ""

#: personal/squid/class_proxyAccount.inc:95
msgid "MinuteStart"
msgstr ""

#: personal/squid/class_proxyAccount.inc:103
msgid "Limit proxy access to working time"
msgstr ""

#: personal/squid/class_proxyAccount.inc:106
msgid "Working Stop"
msgstr ""

#: personal/squid/class_proxyAccount.inc:109
msgid "-"
msgstr ""

#: personal/squid/class_proxyAccount.inc:110
msgid ":"
msgstr ""

#: personal/squid/class_proxyAccount.inc:110
msgid "MinuteStop"
msgstr ""

#: personal/squid/class_proxyAccount.inc:116
#: personal/squid/class_proxyAccount.inc:126
msgid "Restrict proxy usage by quota"
msgstr ""

#: personal/squid/class_proxyAccount.inc:119
msgid "Up"
msgstr ""

#: personal/squid/class_proxyAccount.inc:129
#: personal/squid/class_proxyAccount.inc:130
msgid "per"
msgstr ""

#: personal/squid/class_proxyAccount.inc:133
msgid "hour"
msgstr ""

#: personal/squid/class_proxyAccount.inc:133
msgid "day"
msgstr ""

#: personal/squid/class_proxyAccount.inc:133
msgid "week"
msgstr ""

#: personal/squid/class_proxyAccount.inc:133
msgid "month"
msgstr ""

#: personal/squid/class_proxyAccount.inc:136
msgid "Start Working Hours"
msgstr ""

#: personal/squid/class_proxyAccount.inc:137
msgid "Starting hours for internet access"
msgstr ""

#: personal/squid/class_proxyAccount.inc:145
msgid "Proxy Quota"
msgstr ""

#: personal/squid/class_proxyAccount.inc:146
msgid "Max Data quota for the proxy"
msgstr ""
