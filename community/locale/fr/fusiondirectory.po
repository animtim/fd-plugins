# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# MCMic, 2018
# Benoit Mortier <benoit.mortier@opensides.be>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:51+0000\n"
"Last-Translator: Benoit Mortier <benoit.mortier@opensides.be>, 2018\n"
"Language-Team: French (https://www.transifex.com/fusiondirectory/teams/12202/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: admin/departments/community/class_communityOrganization.inc:28
msgid "Community organization"
msgstr "Organisme communautaire"

#: admin/departments/community/class_communityOrganization.inc:29
msgid "Community organization dates and alternate address"
msgstr "Dates et adresse alternative de l’organisme communautaire"

#: admin/departments/community/class_communityOrganization.inc:42
msgid "Membership"
msgstr "Adhésion"

#: admin/departments/community/class_communityOrganization.inc:45
msgid "Membership type"
msgstr "Type d’adhésion"

#: admin/departments/community/class_communityOrganization.inc:45
msgid "Membership type of this organization"
msgstr "Choix du type d'adhésion de cet organisme"

#: admin/departments/community/class_communityOrganization.inc:50
msgid "Agreement signed"
msgstr "Accord signé"

#: admin/departments/community/class_communityOrganization.inc:50
msgid "Did this member returned the agreement signed"
msgstr "Ce membre a-t-il retourné l'accord signé ?"

#: admin/departments/community/class_communityOrganization.inc:54
msgid "Active"
msgstr "Active"

#: admin/departments/community/class_communityOrganization.inc:54
msgid "Is the membership of this organization active"
msgstr "Est-ce que l’adhésion de cet organisme est active ?"

#: admin/departments/community/class_communityOrganization.inc:58
msgid "Hidden"
msgstr "Cachée"

#: admin/departments/community/class_communityOrganization.inc:58
msgid "Should this membership be hidden from listings"
msgstr "Est-ce que cette adhésion est cachée des listes"

#: admin/departments/community/class_communityOrganization.inc:64
msgid "Dates"
msgstr "Dates"

#: admin/departments/community/class_communityOrganization.inc:67
msgid "Start date"
msgstr "Début"

#: admin/departments/community/class_communityOrganization.inc:67
msgid "Date of the beginning"
msgstr "Date de début"

#: admin/departments/community/class_communityOrganization.inc:71
msgid "End date"
msgstr "Fin"

#: admin/departments/community/class_communityOrganization.inc:71
msgid "Date of the end"
msgstr "Date de fin"

#: admin/departments/community/class_communityOrganization.inc:78
msgid "Contact"
msgstr "Contact"

#: admin/departments/community/class_communityOrganization.inc:81
msgid "First name"
msgstr "Prénom"

#: admin/departments/community/class_communityOrganization.inc:85
msgid "Last name"
msgstr "Nom de famille"

#: admin/departments/community/class_communityOrganization.inc:89
msgid "Address"
msgstr "Adresse"

#: admin/departments/community/class_communityOrganization.inc:89
msgid "A postal address"
msgstr "Adresse postale"

#: admin/departments/community/class_communityOrganization.inc:93
msgid "City"
msgstr "Ville"

#: admin/departments/community/class_communityOrganization.inc:97
msgid "State"
msgstr "Département"

#: admin/departments/community/class_communityOrganization.inc:101
msgid "Country"
msgstr "Pays"

#: admin/departments/community/class_communityProject.inc:28
msgid "Community project"
msgstr "Projet communautaire"

#: admin/departments/community/class_communityProject.inc:29
msgid "Community project dates and alternate address"
msgstr "Dates et adresse alternative du projet communautaire"

#: admin/departments/community/class_communityProject.inc:42
msgid "Project"
msgstr "Projet"

#: admin/departments/community/class_communityProject.inc:45
msgid "Project full name"
msgstr "Nom complet"

#: admin/departments/community/class_communityProject.inc:45
msgid "Full name of this project"
msgstr "Nom complet de ce projet"

#: admin/departments/community/class_communityProject.inc:49
msgid "Project key"
msgstr "Clé de projet"

#: admin/departments/community/class_communityProject.inc:49
msgid "ID used for this project in other softwares or databases"
msgstr "ID utilisé pour ce projet dans d'autres logiciels et bases de données"

#: config/community/class_communityConfig.inc:26
msgid "Community configuration"
msgstr "Configuration de communauté"

#: config/community/class_communityConfig.inc:27
msgid "FusionDirectory community plugin configuration"
msgstr "Configuration du plugin community"

#: config/community/class_communityConfig.inc:40
msgid "Community"
msgstr "Communauté"

#: config/community/class_communityConfig.inc:44
msgid "Membership type choices"
msgstr "Choix de type d'adhésion"

#: config/community/class_communityConfig.inc:44
msgid "Community membership types available in the user community tab"
msgstr ""
"Types d'adhésion disponibles dans l’onglet communauté des utilisateurs"
