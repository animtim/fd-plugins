# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:01+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Latvian (https://www.transifex.com/fusiondirectory/teams/12202/lv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: lv\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);\n"

#: admin/groups/renater-partage/class_partageGroup.inc:28
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:28
msgid "Partage"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:29
msgid "Partage group options"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:43
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:40
msgid "Information"
msgstr "Informācija"

#: admin/groups/renater-partage/class_partageGroup.inc:46
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:48
msgid "Display name"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:46
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:48
msgid "Name to display for this group"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:50
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:52
msgid "Hide from global catalog"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:50
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:52
msgid "Whether this group should be hidden from the global catalog"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:55
msgid "Type"
msgstr "Veids"

#: admin/groups/renater-partage/class_partageGroup.inc:55
msgid "Type of PARTAGE group"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:58
msgid "Distribution list"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:58
msgid "Group"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:61
msgid "Alert new members"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:61
msgid "Send a message to alert new members, they have joined this group"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:66
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:65
msgid "Notes"
msgstr ""

#: admin/groups/renater-partage/class_partageGroup.inc:66
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:65
msgid "Notes about this group"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:26
#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:27
msgid "Renater Partage"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:39
msgid "Settings"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:43
msgid "URI"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:43
msgid "URI to contact the Renater Partage API"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:47
msgid "User Agent"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:47
msgid "User Agent to use to contact the Renater Partage API"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:52
msgid "Mailbox deletion"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:52
msgid ""
"What to do with the PARTAGE account when mail tab is deactivated or user is "
"deleted"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:55
msgid "Delete"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:55
msgid "Disable"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:61
#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:87
msgid "Domains"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:66
msgid "Domains handled by this Renater Partage server"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:70
msgid "Domain"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:70
msgid "Domain handled by this server"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:74
msgid "Key"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:74
msgid "Key for this domain"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:78
msgid "Classes of service"
msgstr ""

#: admin/systems/services/renater-partage/class_serviceRenaterPartage.inc:78
msgid ""
"Possible classes of service for this domain and their ids, separated by "
"commas. Format is cos1Name|cos1Id,cos2Name|cos2Id."
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:29
msgid "Partage sympa options"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:43
msgid "Server"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:43
msgid "Email server"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:70
msgid "Alternative addresses"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:70
msgid "Alternative mail addresses for the list"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:173
#, php-format
msgid "Mail method cannot connect: %s"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:176
#, php-format
msgid "Cannot update mailbox: %s"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:197
#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:201
msgid "Mail error"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:197
#, php-format
msgid "Cannot remove mailbox, mail method cannot connect: %s"
msgstr ""

#: admin/sympa/renater-partage/class_sympaAliasPartage.inc:201
#, php-format
msgid "Cannot remove mailbox: %s"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:64
msgid "Server did not return auth token"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:129
msgid "Partage API answer malformated"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:133
#, php-format
msgid "Partage API Auth failed: %s"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:135
msgid "Partage API Auth failed with no error message"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:144
#, php-format
msgid "Unable to connect to %s: %s"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:146
#, php-format
msgid "Unable to connect to %s"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:172
#, php-format
msgid "Email address \"%s\" does not use the correct partage domain \"%s\""
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:259
msgid "Warning"
msgstr "Brīdinājums"

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:259
#, php-format
msgid ""
"Several addresses were given in gosaMailForwardingAddress but only one is "
"supported by PARTAGE. %s will be sent to PARTAGE."
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:491
msgid "PARTAGE error"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:492
#, php-format
msgid "Several users have uid \"%s\". Ignoring this member."
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:531
#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:544
msgid "An account with the same email address already exists in Partage"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:609
msgid "Invalid value in fdRenaterPartageServerDeletionType"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:646
msgid "Never"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:653
msgid "Last login"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:654
msgid "Account status"
msgstr ""

#: personal/mail/mail-methods/class_mail-methods-renater-partage.inc:654
msgid "Unknown"
msgstr ""
