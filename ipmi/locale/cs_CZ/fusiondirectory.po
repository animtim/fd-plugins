# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:55+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Czech (Czech Republic) (https://www.transifex.com/fusiondirectory/teams/12202/cs_CZ/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs_CZ\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: admin/systems/ipmi/class_ipmiClient.inc:28
msgid "IPMI client"
msgstr "Klient IPMI"

#: admin/systems/ipmi/class_ipmiClient.inc:29
msgid "Edit IPMI client settings"
msgstr "Upravit nastavení IPMI klienta"

#: admin/systems/ipmi/class_ipmiClient.inc:41
msgid "IPMI client settings"
msgstr "Nastavení IPMI klienta"

#: admin/systems/ipmi/class_ipmiClient.inc:44
msgid "IP"
msgstr "IP adresa"

#: admin/systems/ipmi/class_ipmiClient.inc:44
msgid "IP of the IPMI interface"
msgstr "IP adresa IPMI rozhraní"

#: admin/systems/ipmi/class_ipmiClient.inc:48
msgid "User login"
msgstr "Přihlašovací jméno"

#: admin/systems/ipmi/class_ipmiClient.inc:48
msgid "IPMI user login"
msgstr "Přihlašovací jméno k IPMI"

#: admin/systems/ipmi/class_ipmiClient.inc:53
msgid "User password"
msgstr "Heslo uživatele"

#: admin/systems/ipmi/class_ipmiClient.inc:53
msgid "IPMI user password"
msgstr "Heslo uživatele IPMI"
