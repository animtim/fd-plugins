# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:00+0000\n"
"Language-Team: Catalan (https://www.transifex.com/fusiondirectory/teams/12202/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/services/puppet/class_servicePuppet.inc:26
#: admin/systems/services/puppet/class_servicePuppet.inc:41
msgid "Puppet server"
msgstr ""

#: admin/systems/services/puppet/class_servicePuppet.inc:27
msgid "This service allows you to use a puppet server"
msgstr ""

#: admin/systems/services/puppet/class_servicePuppet.inc:45
msgid "Environments"
msgstr ""

#: admin/systems/services/puppet/class_servicePuppet.inc:46
msgid "Available environments for puppet nodes"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:29
msgid "Puppet"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:30
msgid ""
"Support for puppet schema in order to edit puppet classes and puppet vars"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:43
msgid "Puppet node settings"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:46
msgid "Puppet class"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:46
msgid "Puppet Node Class"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:48
msgid "Parent node"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:48
msgid "Puppet Parent Node"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:49
msgid "Environment"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:49
msgid "Puppet Node Environment"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:52
msgid "A variable setting for puppet"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:55
msgid "Name of the variable"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:56
msgid "Value of the variable"
msgstr ""

#: admin/systems/puppet/class_puppetNode.inc:99
msgid ""
"You need to add the puppet service to a server to be able to use this tab"
msgstr ""
