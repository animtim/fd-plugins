# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:54+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Portuguese (https://www.transifex.com/fusiondirectory/teams/12202/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/inventory/class_inventoryManagement.inc:37
msgid "Inventory objects"
msgstr ""

#: admin/inventory/class_inventoryManagement.inc:38
msgid "Browse inventory objects"
msgstr ""

#: admin/inventory/inventory-list.xml:7
msgid "NO LABEL"
msgstr ""

#: admin/inventory/inventory-list.xml:28
#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:68
msgid "IP"
msgstr ""

#: admin/inventory/inventory-list.xml:36
msgid "Macs"
msgstr ""

#: admin/inventory/inventory-list.xml:44
msgid "Operating Systems"
msgstr ""

#: admin/inventory/inventory-list.xml:50
msgid "Actions"
msgstr "Ações"

#: admin/inventory/inventory-list.xml:61 admin/inventory/inventory-list.xml:85
msgid "Edit"
msgstr "Editar"

#: admin/inventory/inventory-list.xml:68 admin/inventory/inventory-list.xml:92
msgid "Remove"
msgstr "Remover"

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:28
#: config/fusioninventory/class_fiConfig.inc:40
msgid "FusionInventory"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:29
msgid "FusionInventory agent configuration"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:42
msgid "Configuration"
msgstr "Configuração"

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:45
msgid "Server"
msgstr "Servidor"

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:45
msgid "Server which FI will send inventory infos"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:49
msgid "Delay time"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:49
msgid "Maximum initial delay before first target, in seconds"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:54
msgid "Wait"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:54
#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:72
msgid "Maximum delay between each targets, in seconds"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:61
msgid "Web Interface"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:64
msgid "Disable web interface"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:64
msgid "Do not use web interface"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:68
msgid "Network interface to listen to"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:72
msgid "Port"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:77
msgid "Trust"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventoryAgent.inc:77
msgid "IPs allowed to launch inventory task through web interface"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:68
msgid "Inventory"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:69
msgid "Inventory Viewer"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:74
#: admin/systems/fusioninventory/class_fiInventory.inc:78
msgid "Inventory object"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:92
msgid "Matching system"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:95
#: admin/systems/fusioninventory/class_fiInventory.inc:377
msgid "System"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:95
#: admin/systems/fusioninventory/class_fiInventory.inc:377
msgid "System entry matching this inventory"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:101
msgid "Hardware"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:107
msgid "Software"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:113
msgid "Users"
msgstr "Usuários"

#: admin/systems/fusioninventory/class_fiInventory.inc:119
msgid "Processes"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:325
msgid "Create a system based on this inventory"
msgstr ""

#: admin/systems/fusioninventory/class_fiInventory.inc:340
msgid "Create"
msgstr "Criar"

#: admin/systems/fusioninventory/class_fiInventory.inc:345
msgid "No matching system"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:26
msgid "FusionInventory configuration"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:27
msgid "FusionDirectory FusionInventory plugin configuration"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:43
msgid "Inventory RDN"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:43
msgid "Branch in which inventory objects will be stored"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:48
msgid "Inventory matching"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:48
msgid "Criteria to link an inventory result to a system"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:51
msgid "Mac address"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:51
msgid "IP address"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:51
msgid "Both"
msgstr ""

#: config/fusioninventory/class_fiConfig.inc:51
msgid "IP or Mac address"
msgstr ""

#: html/collect.php:83
#, php-format
msgid "FusionDirectory configuration %s/%s is not readable. Aborted."
msgstr ""
