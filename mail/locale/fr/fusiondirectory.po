# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:57+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: French (https://www.transifex.com/fusiondirectory/teams/12202/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: admin/groups/mail/class_mailGroup.inc:45
#: config/mail/class_mailPluginConfig.inc:26
#: personal/mail/class_mailAccount.inc:72
msgid "Mail"
msgstr "Courriel"

#: admin/groups/mail/class_mailGroup.inc:46
msgid "Group mail options"
msgstr "Options de courriel du groupe"

#: admin/groups/mail/class_mailGroup.inc:61
msgid "Information"
msgstr "Informations"

#: admin/groups/mail/class_mailGroup.inc:64
#: personal/mail/class_mailAccount.inc:100
msgid "Primary address"
msgstr "Adresse principale"

#: admin/groups/mail/class_mailGroup.inc:64
msgid "The primary mail address"
msgstr "Adresse de courriel principale"

#: admin/groups/mail/class_mailGroup.inc:68
#: personal/mail/class_mailAccount.inc:104
msgid "Server"
msgstr "Serveur"

#: admin/groups/mail/class_mailGroup.inc:68
msgid "Email server"
msgstr "Serveur de courriel"

#: admin/groups/mail/class_mailGroup.inc:74
#: personal/mail/class_mailAccount.inc:120
msgid "Alternative addresses"
msgstr "Adresses alternatives"

#: admin/groups/mail/class_mailGroup.inc:74
msgid "Alternative mail addresses for the group"
msgstr "Adresses de courriel alternatives pour le groupe"

#: admin/groups/mail/class_mailGroup.inc:79
msgid "Forward messages to non group members"
msgstr "Transférer les messages vers des membres n'appartenant pas au groupe"

#: admin/groups/mail/class_mailGroup.inc:83
msgid "Only allowed to receive local mail"
msgstr "Les utilisateurs ne sont autorisés qu'à recevoir des courriel locaux"

#: admin/groups/mail/class_mailGroup.inc:83
msgid ""
"Whether this group mail is only allowed to receive messages from local "
"senders"
msgstr ""
"Ce groupe est uniquement autorisé à recevoir des messages provenant "
"d'expéditeurs locaux"

#: admin/groups/mail/class_mailGroup.inc:192
#: personal/mail/class_mailAccount.inc:268
#: personal/mail/class_mailAccount.inc:369
#, php-format
msgid "Mail method cannot connect: %s"
msgstr "Le méthode de courriel ne peut pas se connecter : %s"

#: admin/groups/mail/class_mailGroup.inc:195
#: personal/mail/class_mailAccount.inc:372
#, php-format
msgid "Cannot update mailbox: %s"
msgstr "Impossible de mettre à jour le compte : %s"

#: admin/groups/mail/class_mailGroup.inc:216
#: admin/groups/mail/class_mailGroup.inc:220
#: personal/mail/class_mailAccount.inc:45
#: personal/mail/class_mailAccount.inc:268
#: personal/mail/class_mailAccount.inc:271
#: personal/mail/class_mailAccount.inc:375
#: personal/mail/class_mailAccount.inc:386
#: personal/mail/class_mailAccount.inc:415
#: personal/mail/class_mailAccount.inc:419
msgid "Mail error"
msgstr "Erreur du serveur de courriel"

#: admin/groups/mail/class_mailGroup.inc:216
#: personal/mail/class_mailAccount.inc:415
#, php-format
msgid "Cannot remove mailbox, mail method cannot connect: %s"
msgstr ""
"Impossible de supprimer la boite de courriel, impossible de se connecter : "
"%s"

#: admin/groups/mail/class_mailGroup.inc:220
#: personal/mail/class_mailAccount.inc:419
#, php-format
msgid "Cannot remove mailbox: %s"
msgstr "Impossible de supprimer la boite de courriel : %s"

#: admin/systems/services/imap/class_serviceIMAP.inc:53
msgid "IMAP/POP3 generic service"
msgstr "Service IMAP/POP3 générique"

#: admin/systems/services/imap/class_serviceIMAP.inc:54
msgid "IMAP/POP3"
msgstr "IMAP/POP3"

#: admin/systems/services/imap/class_serviceIMAP.inc:54
msgid "Services"
msgstr "Services"

#: config/mail/class_mailPluginConfig.inc:27
msgid "Mail plugin configuration"
msgstr "Configuration du plugin mail"

#: config/mail/class_mailPluginConfig.inc:40
#: personal/mail/class_mailAccount.inc:73
msgid "Mail settings"
msgstr "Paramètres de courriel"

#: config/mail/class_mailPluginConfig.inc:43
msgid "Account identification attribute"
msgstr "Attribut d'identification du compte"

#: config/mail/class_mailPluginConfig.inc:44
msgid "Which attribute will be used to create accounts."
msgstr "Quel attribut sera utilisé pour créer des comptes."

#: config/mail/class_mailPluginConfig.inc:49
msgid "Mail user template"
msgstr "Modèle de compte de courriel"

#: config/mail/class_mailPluginConfig.inc:50
msgid "Override the user account creation syntax."
msgstr "Remplace la syntaxe de création d'un compte utilisateur."

#: config/mail/class_mailPluginConfig.inc:54
msgid "Mail folder template"
msgstr "Modèle de dossier de courriel"

#: config/mail/class_mailPluginConfig.inc:55
msgid "Override the methods default account creation syntax."
msgstr "Remplace la syntaxe de création de compte par défaut"

#: config/mail/class_mailPluginConfig.inc:59
msgid "Use cyrus UNIX style"
msgstr "Utiliser le style Cyrus UNIX"

#: config/mail/class_mailPluginConfig.inc:60
msgid ""
"Determines if 'foo/bar' or 'foo.bar' should be uses as namespaces in IMAP."
msgstr ""
"Détermine si \"foo/bar\" ou \"foo.bar\" doit être utilisé comme espace de "
"nom IMAP."

#: config/mail/class_mailPluginConfig.inc:64
msgid "Delete mailbox on account deletion"
msgstr "Supprimer la boite de courriel lors de la suppression du compte"

#: config/mail/class_mailPluginConfig.inc:65
msgid ""
"Determines if the mailbox should be removed from your IMAP server after the "
"account is deleted in LDAP."
msgstr ""
"Détermine si la boîte de courriel doit être retirée de votre serveur IMAP "
"après la suppression du compte LDAP."

#: config/mail/class_mailPluginConfig.inc:70
msgid "Cyrus autocreate folders"
msgstr "Création automatique des dossiers Cyrus"

#: config/mail/class_mailPluginConfig.inc:71
msgid ""
"List of personal IMAP folders that should be created along initial account "
"creation."
msgstr ""
"Liste des dossiers IMAP personnels qui doivent être créés lors de la "
"création du compte initial."

#: config/mail/class_mailPluginConfig.inc:76
msgid "IMAP timeout"
msgstr "Délai d'inactivité IMAP"

#: config/mail/class_mailPluginConfig.inc:77
msgid "Sets the connection timeout for imap actions."
msgstr "Définit le délai d’attente maximum pour les actions IMAP."

#: config/mail/class_mailPluginConfig.inc:82
msgid "Shared prefix"
msgstr "Préfixe des dossiers partagés"

#: config/mail/class_mailPluginConfig.inc:83
msgid "Prefix to add for mail shared folders."
msgstr "Préfixe à ajouter pour les dossiers partagés."

#: personal/mail/class_mailAccount.inc:45
#, php-format
msgid "Cannot read quota settings: %s"
msgstr "Impossible de lire les paramètres de quota : %s"

#: personal/mail/class_mailAccount.inc:55
msgid "Quota usage"
msgstr "Utilisation du quota"

#: personal/mail/class_mailAccount.inc:56
msgid "Part of the quota which is used"
msgstr "Part du quota qui est utilisée"

#: personal/mail/class_mailAccount.inc:97
msgid "Mail account"
msgstr "Compte courriel"

#: personal/mail/class_mailAccount.inc:100
msgid "Primary mail address"
msgstr "Adresse de courriel principale"

#: personal/mail/class_mailAccount.inc:104
msgid "Specify the mail server where the user will be hosted on"
msgstr "Indiquez le serveur de courriel pour cet utilisateur"

#: personal/mail/class_mailAccount.inc:109
msgid "Quota size"
msgstr "Taille du quota"

#: personal/mail/class_mailAccount.inc:116
msgid "Other addresses and redirections"
msgstr "Autres adresses et redirections"

#: personal/mail/class_mailAccount.inc:120
msgid "List of alternative mail addresses"
msgstr "Liste des adresses de courriel alternatives"

#: personal/mail/class_mailAccount.inc:125
msgid "Forward messages to"
msgstr "Transférer les messages vers"

#: personal/mail/class_mailAccount.inc:125
msgid "Addresses to which messages should be forwarded"
msgstr "Adresses vers laquelle les messages doivent être redirigés"

#: personal/mail/class_mailAccount.inc:131
#: personal/mail/class_mailAccount.inc:150
msgid "Vacation message"
msgstr "Message d'absence"

#: personal/mail/class_mailAccount.inc:134
msgid "Activate vacation message"
msgstr "Activer le message d'absence"

#: personal/mail/class_mailAccount.inc:135
msgid ""
"Select to automatically response with the vacation message defined below"
msgstr "Réponse automatique par le message d'absence ci-dessous"

#: personal/mail/class_mailAccount.inc:140
msgid "from"
msgstr "du"

#: personal/mail/class_mailAccount.inc:145
msgid "till"
msgstr "jusqu'au"

#: personal/mail/class_mailAccount.inc:156
msgid "Advanced mail options"
msgstr "Options de courriel avancées"

#: personal/mail/class_mailAccount.inc:164
msgid "User is only allowed to send and receive local mails"
msgstr ""
"Les utilisateurs ne sont autorisés qu'à envoyer et recevoir des courriel "
"locaux"

#: personal/mail/class_mailAccount.inc:165
msgid "Select if user can only send and receive inside his own domain"
msgstr ""
"Sélectionnez si vous voulez que les utilisateurs puissent envoyer et "
"recevoir des courriels uniquement dans leur propre domaine"

#: personal/mail/class_mailAccount.inc:170
msgid "No delivery to own mailbox"
msgstr "Aucune distribution dans sa propre boite"

#: personal/mail/class_mailAccount.inc:171
msgid "Select if you want to forward mails without getting own copies of them"
msgstr ""
"Sélectionnez ceci si vous souhaitez relayer les courriels sans garder de "
"copie de ceux-ci"

#: personal/mail/class_mailAccount.inc:271
#, php-format
msgid "Mailbox \"%s\" doesn't exists on mail server: %s"
msgstr "Le compte de courriel «%s» n'existe pas sur le serveur : %s"

#: personal/mail/class_mailAccount.inc:375
#, php-format
msgid "Cannot write quota settings: %s"
msgstr "Impossible d'écrire les paramètres de quota : %s"

#: personal/mail/class_mailAccount.inc:386
#, php-format
msgid "Mail error saving sieve settings: %s"
msgstr "Erreur lors de l'enregistrement des paramètres Sieve : %s"

#: personal/mail/class_mail-methods.inc:148
msgid "Configuration error"
msgstr "Erreur de configuration"

#: personal/mail/class_mail-methods.inc:149
#, php-format
msgid "The configured mail attribute '%s' is unsupported!"
msgstr "L'attribut de courriel \"%s\" n'est pas supporté !"

#: personal/mail/class_mail-methods.inc:738
msgid "Unknown"
msgstr "Inconnu"

#: personal/mail/class_mail-methods.inc:740
msgid "Unlimited"
msgstr "Illimité"
