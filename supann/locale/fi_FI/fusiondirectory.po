# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-11-20 15:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:04+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Finnish (Finland) (https://www.transifex.com/fusiondirectory/teams/12202/fi_FI/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fi_FI\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/supannStructures/class_etablissement.inc:26
#: personal/supann/class_supannAccount.inc:335
#: personal/supann/class_supannAccount.inc:374
msgid "Establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:27
msgid "SupAnn Establishment Settings"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:31
msgid "SupAnn Establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:48
msgid "Properties"
msgstr "Ominaisuudet"

#: admin/supannStructures/class_etablissement.inc:51
msgid "Root establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:51
msgid "Set this establishment as the root one"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:55
#: admin/supannStructures/class_entite.inc:50
msgid "Name"
msgstr "Nimi"

#: admin/supannStructures/class_etablissement.inc:55
msgid "The name to write in the o attribute for this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:59
#: admin/supannStructures/class_entite.inc:71
msgid "Description"
msgstr "Kuvaus"

#: admin/supannStructures/class_etablissement.inc:59
msgid "A short description of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:65
#: admin/supannStructures/class_etablissement.inc:68
msgid "Location"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:68
msgid "Usually the city where this establishment is"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:72
msgid "Address"
msgstr "Osoite"

#: admin/supannStructures/class_etablissement.inc:72
msgid "The postal address of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:76
#: admin/supannStructures/class_entite.inc:59
msgid "Telephone"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:76
msgid "Phone number of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:80
#: admin/supannStructures/class_entite.inc:63
msgid "Fax"
msgstr "Faksi"

#: admin/supannStructures/class_etablissement.inc:80
msgid "Fax number of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:86
msgid "SupAnn properties"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:89
msgid "Establishment code"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:89
msgid "The code of this establishment (must have a prefix between {})"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:94
msgid "SupAnn code"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:94
msgid "The SupAnn code for this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:99
msgid "Legal name"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:99
msgid "The legal name of this establishment"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:103
msgid "Home page URI"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:103
msgid "The URI of this establishment website home page"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:107
msgid "Institution URI"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:107
msgid "The URI of this establishment institution website"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:111
msgid "White pages URI"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:111
msgid "The URI of this establishment white pages"
msgstr ""

#: admin/supannStructures/class_etablissement.inc:155
#: admin/supannStructures/class_etablissement.inc:222
msgid "LDAP error"
msgstr "LDAP virhe"

#: admin/supannStructures/class_supannStructuresManagement.inc:37
msgid "SupAnn structures"
msgstr ""

#: admin/supannStructures/class_supannStructuresManagement.inc:38
msgid "SupAnn structures management"
msgstr ""

#: admin/supannStructures/class_supann.inc:53
msgid "File error"
msgstr ""

#: admin/supannStructures/class_supann.inc:54
#, php-format
msgid "Cannot read file: '%s'"
msgstr ""

#: admin/supannStructures/class_entite.inc:26
#: admin/supannStructures/class_entite.inc:47
#: personal/supann/class_supannAccount.inc:463
msgid "Entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:27
msgid "SupAnn Entity Settings"
msgstr ""

#: admin/supannStructures/class_entite.inc:30
msgid "SupAnn Entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:50
msgid "The name to write in the ou attribute for this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:56
msgid "Administrative information"
msgstr ""

#: admin/supannStructures/class_entite.inc:59
msgid "Phone number of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:63
msgid "Fax number of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:67
msgid "Postal address"
msgstr ""

#: admin/supannStructures/class_entite.inc:67
msgid "Postal address of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:71
msgid "Short description of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:77
msgid "SupAnn information"
msgstr ""

#: admin/supannStructures/class_entite.inc:80
#: personal/supann/class_supannAccount.inc:459
msgid "Entity type"
msgstr ""

#: admin/supannStructures/class_entite.inc:80
msgid "The SupAnn type that best fits this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:84
msgid "Entity code"
msgstr ""

#: admin/supannStructures/class_entite.inc:84
msgid "The SupAnn code of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:89
msgid "Parent entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:89
msgid "The parent entity of this entity"
msgstr ""

#: admin/supannStructures/class_entite.inc:94
msgid "Reference IDs"
msgstr ""

#: admin/supannStructures/class_entite.inc:94
msgid "supannRefId - IDs/links for this entity on other systems"
msgstr ""

#: config/supann/class_supannConfig.inc:26
msgid "SupAnn configuration"
msgstr ""

#: config/supann/class_supannConfig.inc:27
msgid "FusionDirectory SupAnn plugin configuration"
msgstr ""

#: config/supann/class_supannConfig.inc:40
#: personal/supann/class_supannAccount.inc:222
msgid "SupAnn"
msgstr ""

#: config/supann/class_supannConfig.inc:43
msgid "SupAnn RDN"
msgstr ""

#: config/supann/class_supannConfig.inc:43
msgid "Branch in which SupAnn structures will be stored"
msgstr ""

#: config/supann/class_supannConfig.inc:48
msgid "SupAnn mail for recovery"
msgstr ""

#: config/supann/class_supannConfig.inc:48
msgid ""
"Allow the use of mail addresses from the personal mail address field from "
"SupAnn account for password recovery"
msgstr ""

#: personal/supann/class_supannAccount.inc:36
#: personal/supann/class_supannAccount.inc:119
msgid "None"
msgstr ""

#: personal/supann/class_supannAccount.inc:116
msgid "Licence"
msgstr ""

#: personal/supann/class_supannAccount.inc:116
msgid "Master"
msgstr ""

#: personal/supann/class_supannAccount.inc:116
msgid "Ph.D."
msgstr ""

#: personal/supann/class_supannAccount.inc:116
msgid "Another class of degree"
msgstr ""

#: personal/supann/class_supannAccount.inc:116
msgid "Post-graduate year"
msgstr ""

#: personal/supann/class_supannAccount.inc:119
msgid "1st year"
msgstr ""

#: personal/supann/class_supannAccount.inc:119
msgid "2nd year"
msgstr ""

#: personal/supann/class_supannAccount.inc:119
msgid "3rd year"
msgstr ""

#: personal/supann/class_supannAccount.inc:120
msgid "4th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:120
msgid "5th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:120
msgid "6th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:121
msgid "7th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:121
msgid "8th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:121
msgid "9th year"
msgstr ""

#: personal/supann/class_supannAccount.inc:223
msgid "SupAnn information management plugin"
msgstr ""

#: personal/supann/class_supannAccount.inc:242
msgid "Identity"
msgstr ""

#: personal/supann/class_supannAccount.inc:245
msgid "Civilite"
msgstr ""

#: personal/supann/class_supannAccount.inc:245
msgid "supannCivilite - Civility for this person"
msgstr ""

#: personal/supann/class_supannAccount.inc:250
msgid "Alias login"
msgstr ""

#: personal/supann/class_supannAccount.inc:250
msgid "supannAliasLogin - An alias for the login of this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:254
msgid "eduPersonPrincipalName"
msgstr ""

#: personal/supann/class_supannAccount.inc:254
msgid ""
"eduPersonPrincipalName - A name that looks like <id>@<domain> which is "
"unique for this domain, and has not be assigned to anyone else recently"
msgstr ""

#: personal/supann/class_supannAccount.inc:258
msgid "Nickname"
msgstr ""

#: personal/supann/class_supannAccount.inc:258
msgid "eduPersonNickname - Can contain a nickname for this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:263
msgid "Ref ids"
msgstr ""

#: personal/supann/class_supannAccount.inc:263
msgid "supannRefId - IDs/links for this user on other systems"
msgstr ""

#: personal/supann/class_supannAccount.inc:273
msgid "Contact"
msgstr "Yhteystiedot"

#: personal/supann/class_supannAccount.inc:277
msgid "Other phone numbers"
msgstr ""

#: personal/supann/class_supannAccount.inc:277
msgid "supannAutreTelephone - Other phone numbers for this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:283
msgid "Other mail addresses"
msgstr ""

#: personal/supann/class_supannAccount.inc:283
msgid ""
"supannAutreMail - Other mail addresses for this user. Each must be unique"
msgstr ""

#: personal/supann/class_supannAccount.inc:289
msgid "Personal mail addresses"
msgstr ""

#: personal/supann/class_supannAccount.inc:289
msgid "supannMailPerso - Personal mail addresses for this user"
msgstr ""

#: personal/supann/class_supannAccount.inc:294
msgid "Red list"
msgstr ""

#: personal/supann/class_supannAccount.inc:294
msgid "supannListeRouge - Should this person be on the red list"
msgstr ""

#: personal/supann/class_supannAccount.inc:300
msgid "Assignment"
msgstr ""

#: personal/supann/class_supannAccount.inc:303
msgid "Primary assignment"
msgstr ""

#: personal/supann/class_supannAccount.inc:303
msgid "supannEntiteAffectationPrincipale - Main assignment of the person"
msgstr ""

#: personal/supann/class_supannAccount.inc:308
msgid "Assignments"
msgstr ""

#: personal/supann/class_supannAccount.inc:308
#: personal/supann/class_supannAccount.inc:463
msgid ""
"supannEntiteAffectation - Represents assignments of the person in an "
"institution, a component, service, etc."
msgstr ""

#: personal/supann/class_supannAccount.inc:314
msgid "Entity types"
msgstr ""

#: personal/supann/class_supannAccount.inc:314
msgid ""
"supannTypeEntiteAffectation - Types of the entities this person is assigned "
"to"
msgstr ""

#: personal/supann/class_supannAccount.inc:321
msgid "Affiliation"
msgstr ""

#: personal/supann/class_supannAccount.inc:324
msgid "Primary affiliation"
msgstr ""

#: personal/supann/class_supannAccount.inc:324
msgid "eduPersonPrimaryAffiliation - Main status of the person"
msgstr ""

#: personal/supann/class_supannAccount.inc:329
msgid "Affiliations"
msgstr ""

#: personal/supann/class_supannAccount.inc:329
msgid ""
"eduPersonAffiliation - Status of the person: student, BIATOSS, teacher, "
"contract, retired, hosted staff (CNRS, INSERM, etc.), a former student, etc."
msgstr ""

#: personal/supann/class_supannAccount.inc:335
msgid ""
"supannEtablissement - Institution or unit of administrative attachment of "
"the person"
msgstr ""

#: personal/supann/class_supannAccount.inc:342
msgid "Student profile"
msgstr ""

#: personal/supann/class_supannAccount.inc:345
msgid "INE code"
msgstr ""

#: personal/supann/class_supannAccount.inc:345
msgid "supannCodeINE - INE code of this student"
msgstr ""

#: personal/supann/class_supannAccount.inc:349
msgid "Student ID"
msgstr ""

#: personal/supann/class_supannAccount.inc:349
msgid "supannEtuId - Scolarity id"
msgstr ""

#: personal/supann/class_supannAccount.inc:355
msgid "Student registrations"
msgstr ""

#: personal/supann/class_supannAccount.inc:370
msgid "supannEtuInscription - Registrations for this student"
msgstr ""

#: personal/supann/class_supannAccount.inc:374
msgid ""
"supannEtablissement - Etablissement in which this registration was done"
msgstr ""

#: personal/supann/class_supannAccount.inc:378
msgid "Year"
msgstr ""

#: personal/supann/class_supannAccount.inc:378
msgid "supannEtuAnneeInscription - The year this registration will begin"
msgstr ""

#: personal/supann/class_supannAccount.inc:383
msgid "Registration type"
msgstr ""

#: personal/supann/class_supannAccount.inc:383
msgid "supannEtuRegimeInscription - The type of this registration"
msgstr ""

#: personal/supann/class_supannAccount.inc:387
msgid "Disciplinary Sector"
msgstr ""

#: personal/supann/class_supannAccount.inc:387
msgid "supannEtuSecteurDisciplinaire - Disciplinary sector education diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:391
msgid "Diploma type"
msgstr ""

#: personal/supann/class_supannAccount.inc:391
msgid "supannEtuTypeDiplome - Type of diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:395
msgid "Curriculum year "
msgstr ""

#: personal/supann/class_supannAccount.inc:395
msgid ""
"supannEtuCursusAnnee - Type of curriculum (L, M, D or X, ...) and the year "
"in the diploma."
msgstr ""

#: personal/supann/class_supannAccount.inc:399
msgid "Entity assignment"
msgstr ""

#: personal/supann/class_supannAccount.inc:399
msgid "supannEntiteAffectation"
msgstr ""

#: personal/supann/class_supannAccount.inc:403
msgid "Diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:403
msgid "supannEtuDiplome - Diploma prepared by the student"
msgstr ""

#: personal/supann/class_supannAccount.inc:407
msgid "Step"
msgstr ""

#: personal/supann/class_supannAccount.inc:407
msgid ""
"supannEtuEtape - Step can be considered a split (semester, year, etc.) in "
"time of education leading to a diploma"
msgstr ""

#: personal/supann/class_supannAccount.inc:411
msgid "educational element"
msgstr ""

#: personal/supann/class_supannAccount.inc:411
msgid ""
"supannEtuElementPedagogique - Generic description of the content of "
"education with a high level of granularity"
msgstr ""

#: personal/supann/class_supannAccount.inc:425
msgid "Personal profile"
msgstr ""

#: personal/supann/class_supannAccount.inc:428
msgid "Personal ID"
msgstr ""

#: personal/supann/class_supannAccount.inc:428
msgid "supannEmpId - Employee identifier"
msgstr ""

#: personal/supann/class_supannAccount.inc:432
msgid "Personal corps"
msgstr ""

#: personal/supann/class_supannAccount.inc:432
msgid "supannEmpCorps"
msgstr ""

#: personal/supann/class_supannAccount.inc:437
msgid "Activity"
msgstr ""

#: personal/supann/class_supannAccount.inc:437
msgid "supannActivite - Category of profession"
msgstr ""

#: personal/supann/class_supannAccount.inc:444
msgid "Roles"
msgstr ""

#: personal/supann/class_supannAccount.inc:451
msgid "supannRoleEntite"
msgstr ""

#: personal/supann/class_supannAccount.inc:455
msgid "Generic role"
msgstr ""

#: personal/supann/class_supannAccount.inc:455
msgid "supannRoleGenerique - Generic role of the person in the facility"
msgstr ""

#: personal/supann/class_supannAccount.inc:459
msgid "supannTypeEntiteAffectation - type of the assigned entity"
msgstr ""

#: personal/supann/class_supannAccount.inc:569
msgid ""
"\"member\" and \"affiliate\" values are incompatible for "
"eduPersonAffiliation. Please remove one of them."
msgstr ""
